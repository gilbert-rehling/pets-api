<?php

/**
 * This makes our life easier when dealing with paths. Everything is relative
 * to the application root now.
 */
chdir(dirname(__DIR__));

// Decline static file requests back to the PHP built-in webserver
if (php_sapi_name() === 'cli-server' || php_sapi_name() === 'cli') {
    if (isset($_SERVER['REQUEST_URI'])) {
        $path = realpath(__DIR__ . parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
    } else {
        $path = realpath(__DIR__ . parse_url($_SERVER['PHP_SELF'], PHP_URL_PATH));
    }

    if (__FILE__ !== $path && is_file($path)) {
        return false;
    }
    unset($path);
}

// Composer auto-loading
include __DIR__ . '/../vendor/autoload.php';

/**
 * @uses Models
 */
use \Application\Models\Pet;
use Application\Models\Store;
use Application\Models\User;

// Retrieve configuration
$appConfig = require __DIR__ . '/../config/application.config.php';

// $routePrefix = '/api/v1';

// define the routes URI's we will need here
$dispatcher = FastRoute\simpleDispatcher(function(FastRoute\RouteCollector $r) {

    /** Sample **/
    // {id} must be an integer (\d+)
    // $r->addRoute('GET', '/uri/{id:\d+}', 'IndexApiController');

    $r->addGroup('/api/v1', function(\FastRoute\RouteCollector $r) {

        /** Pet */
        $r->addRoute('GET', '/pet/{id:\d+}', 'PetApiController');

        $r->addRoute('GET', '/pet/findByStatus', 'PetApiController');

        $r->addRoute('DELETE', '/pet/{id:\d+}', 'PetApiDeleteController');

        $r->addRoute('POST', '/pet/{id:\d+}', 'PetApiPostController');

        $r->addRoute('POST', '/pet/{id:\d+}/uploadImage', 'PetApiPostController');

        $r->addRoute('POST', '/pet', 'PetApiPostController');

        $r->addRoute('PUT', '/pet', 'PetApiPutController');

        /** Store */
        $r->addRoute('GET', '/store/inventory', 'StoreApiController');

        $r->addRoute('GET', '/store/order/{id: \d+}', 'StoreApiController');

        $r->addRoute('DELETE', '/store/{id: \d+}', 'StoreApiDeleteController');

        $r->addRoute('POST', '/store/order', 'StoreApiPostController');

        /** User */
        $r->addRoute('GET', '/user/login', 'UserApiController');

        $r->addRoute('GET', '/user/logout', 'UserApiController');

        $r->addRoute('GET', '/user/{username}', 'UserApiController');

        $r->addRoute('DELETE', '/user/{username}', 'UserApiDeleteController');

        $r->addRoute('POST', '/user', 'UserApiPostController');

        $r->addRoute('POST', '/user/createWithArray', 'UserApiPostController');

        $r->addRoute('POST', '/user/createWithList', 'UserApiPostController');

        $r->addRoute('PUT', '/user/{username}', 'UserApiPutController');
    });
});

// Fetch method and URI from somewhere
$httpMethod = $_SERVER['REQUEST_METHOD'];
$uri = $_SERVER['REQUEST_URI'];

// Strip query string (?foo=bar) and decode URI
if (false !== $pos = strpos($uri, '?')) {
    $uri = substr($uri, 0, $pos);
}
$uri = rawurldecode($uri);

// define the namespaces so we can load the remote classes
$nameSpaces = array(
    'PetApiController' => 'Api\\Pet\\Controllers',
    'PetApiDeleteController' => 'Api\\Pet\\Controllers',
    'PetApiPostController' => 'Api\\Pet\\Controllers',
    'PetApiPutController' => 'Api\\Pet\\Controllers',
    'StoreApiController' => 'Api\\Store\\Controllers',
    'StoreApiDeleteController' => 'Api\\Store\\Controllers',
    'StoreApiPostController' => 'Api\\Store\\Controllers',
    'UserApiController' => 'Api\\User\\Controllers',
    'UserApiDeleteController' => 'Api\\User\\Controllers',
    'UserApiPostController' => 'Api\\User\\Controllers'
);

// configure the POD library
if (file_exists(__DIR__ . '/../config/db.json')) {
    Phormium\DB::configure(__DIR__ . '/../config/db.json');
}

// process the route info
$application = false;
$routeInfo   = $dispatcher->dispatch($httpMethod, $uri);

// we need to pass any query parameter to the controllers
$queryParams = parse_url($_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], PHP_URL_QUERY);
$params = null;
if ($queryParams) {
    $queryParams = explode("&", $queryParams);
    $params = [];
    foreach ($queryParams as $query) {
        $arr        = explode("=", $query);
        $params[$arr[0]] = $arr[1];
    }
}

// function used to return errors to the client
function returnError($code, $message, $type) {
    $return = [
        'code' => $code,
        'message' => $message,
        'type' => $type
    ];
    header('Content-Type: application/json');
    echo json_encode($return);
    // without this exit an invalid response is returned
    exit;
}

// sets up the models for each controller
function getModels( $handler ) {
    $models = [];
    switch($handler) {
        case 'PetApiController':
        case 'PetApiDeleteController':
        case 'PetApiPostController':
        case 'PetApiPutController':
            $models['pet'] = Pet::objects();
            break;

        case 'StoreApiController':
        case 'StoreApiDeleteController':
        case 'StoreApiPostController':
            $models['store'] = Store::objects();
            $models['pet'] = Pet::objects();
            break;

        case 'UserApiController':
        case 'UserApiDeleteController':
        case 'UserApiPostController':
            $models['user'] = User::objects();
            break;
    }
    return $models;
}

switch ($routeInfo[0]) {
    case FastRoute\Dispatcher::NOT_FOUND:
        // ... 404 Not Found
        //header("Location: /404.html");
        returnError(404, 'The specified resource does not exist.', 'Not Found');
        break;

    case FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
        $allowedMethods = $routeInfo[1];
        // ... 405 Method Not Allowed
        returnError(405, 'HTTP Method is not permitted', 'Method Not Allowed');
        break;

    case FastRoute\Dispatcher::FOUND:
        $handler = $routeInfo[1];
        $vars = $routeInfo[2];

        // ... call $handler with $vars
        $className = "\\$nameSpaces[$handler]\\$handler";

        // get the DB Models and pass to each controller for injection
        $pdo = getModels( $handler );

        try {
            // instantiate the class
            $application = new $className( $appConfig, $pdo, $vars, $params );
        }
        catch (\Exception $e) {
            returnError(500, 'Server error: ' . $e->message(), 'Internal Server Error');
        }
        break;
}

// call the default method
$application->indexAction();