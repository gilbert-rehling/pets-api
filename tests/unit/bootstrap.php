<?php

/**
 * Test suite bootstrap
 *
 */

error_reporting('^E_NOTICE');

set_include_path('.:/usr/share/pear:/usr/share/php:/var/hosting/sites/mypalpetapi/microservice');

// Application Path - root dir of this instance
define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../../'));

// test data name
defined('DB_TEST') || define('DB_TEST', 'petapi_test');

// load the test db configuration
if (file_exists(APPLICATION_PATH . '/config/db_test.json')) {
    Phormium\DB::configure(APPLICATION_PATH . '/config/db_test.json');
}

require_once(APPLICATION_PATH . '/vendor/autoload.php');

