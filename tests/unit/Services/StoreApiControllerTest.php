<?php

// namespace unit\Services;

error_reporting('^E_NOTICE');

use Api\Store\Controllers\StoreApiController as StoreApiController;

use PHPUnit\Framework\TestCase as PHPUnit_Framework_TestCase;

use Phormium\Connection as Connection;

use Application\Models\Store as Store;

use Application\Models\Pet as Pet;

class Test_Service_StoreApiControllerTest extends PHPUnit_Framework_TestCase
{
    /**
     * Application configuration
     *
     * @var Config
     */
    private $config = NULL;

    /**
     * @var StoreApiController
     */
    private $storeApiController;

    /**
     * Faked PDO class
     * '
     * @var array
     */
    private $pdo;

    /**
     * Faked store data
     * '
     * @var array
     */
    private $store;

    /**
     * @throws \Exception
     */
    public function setUp(): void
    {
        // not really needed as we have the bootstrap.php - but just in case
        if ($this->config == NULL) {
            $this->config = include APPLICATION_PATH . '/config/application.config.php';
        }

        // dummy store data
        $today     = new \DateTime(date('Y-m-d H:i:s', time()));
        $_5daysago = new \DateTime(date('Y-m-d H:i:s', time() - 60*60*24*5));
        $_9daysago = new \DateTime(date('Y-m-d H:i:s', time() - 60*60*24*9));
        $this->store = array(
            'id'            => '10',
            'pet_id'        => '3',
            'quantity'      => '0',
            'ship_date'     => $_5daysago->format('Y-m-d H:i:s.u'),
            'status'        => 'delivered',
            'completed'     => '1',
            'updated_at'    => $today->format('Y-m-d H:i:s.u'),
            'created_at'    => $_9daysago->format('Y-m-d H:i:s.u')
        );

        // ToDo: this can be reused for each test script
        $store = new Store();
        $store->overrideDbName(DB_TEST);
        $pet = new Pet();
        $pet->overrideDbName(DB_TEST);
        $arr = array(
            'store' => $store::objects(),
            'pet' => $pet::objects()
        );

        // ToDo: didnt work for me :(
//        $this->pdo = \Mockery::mock(Connection::class);
//        $this->pdo->shouldReceive('inTransaction')
//            ->once()
//            ->andReturn(false);

        // setup the class we are testing
        $this->storeApiController   = new StoreApiController($this->config, $arr, null, null);

    }

    /**
     * Generic error checking test
     */
    public function testEmpty()
    {
        $result = '';
        $this->assertEquals(null, $result);
    }

    public function testFindById()
    {
        $id = 10;
//        $storeController = \Phake::mock(StoreApiController::class);


        $store = Store::fromArray( $this->store )->save();
//
//        \Phake::when($storeController)->setId( $id )->thenReturn( $id);
//        $this->storeApiController->setId($id);

        // ToDo: this wouldnt work? - tried using Phake too! Some quirk with the model abstract perhaps?
//        $this->pdo->shouldReceive('preparedQuery')->once()
//            ->with('/^SELECT /', [$id], \PDO::FETCH_CLASS, Store::class)
//            ->andReturn([$store]);
//
//        \Phake::when($storeController)->findById($id)->thenReturn($store);

         $result = $this->storeApiController->findById($id);
     //   $result = $this->storeApiController->indexAction();

        $this->assertEquals(3, $result['pet_id']);
    }

    public function testFindByStatus()
    {
        $results = $this->storeApiController->findByStatus();

        $this->assertCount(3, $results);
    }

    public function testIndexAction()
    {
        $results = $this->storeApiController->indexAction();

        $this->assertJson( $results );
    }
}