#!/bin/bash
FNAME=$1
cd /var/hosting/sites/mypalpetapi/microservice/tests/
if [ $FNAME == ${FNAME##*/tests/} ];
then
    ../vendor/bin/phpunit
else
    ../vendor/bin/phpunit ${FNAME##*/tests/}
fi