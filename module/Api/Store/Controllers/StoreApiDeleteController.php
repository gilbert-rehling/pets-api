<?php

/**
 * Namespace
 */
namespace Api\Store\Controllers;

/**
 * @uses
 */
use Application\Models\Store;
use Exception;
use Application\Controllers\AbstractRestController;

/**
 * Class StoreApiDeleteController
 * @package Api\Store\Controllers
 */
class StoreApiDeleteController extends AbstractRestController
{
    /**
     * @var $config array (database configuration)
     */
    private $config;

    /**
     * @var Store
     */
    private $store;

    /**
     * @var $id integer
     */
    private $id = null;

    /**
     * @var array|null
     */
    private $params = null;

    /**
     * @param $id
     */
    public function setId( $id )
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $params
     */
    public function setParams( $params )
    {
        $this->params = $params;
    }

    /**
     * @return array|null
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * StoreApiDeleteController constructor.
     *
     * @param array|null    $config
     * @param array|null    $pdo
     * @param array|null    $vars
     * @param array|null    $params
     *
     * @throws Exception
     */
    public function __construct( $config = null, $pdo = null, $vars = null, $params = null )
    {
        if ($config == null) {
            throw new \Exception('No configuration was passed to the constructor');
        }
        $this->config = $config;

        if ($pdo == null) {
            throw new \Exception('Database ORM not available or not configured');
        }
        $this->pet   = $pdo['pet'];
        $this->store = $pdo['store'];

        if ($vars) {
            // !! only accept int as value !!
            $this->setId((int) $vars['id']);
        }

        if ($params) {
            $this->setParams( $params );
        }
    }

    /**
     * Index action runs method selection
     */
    public function indexAction()
    {
        try {
            // call the correct method
            if ($this->id) {
                $data = $this->deleteById();

            } else {
                $data = [];
            }

            // prepare and send the response
            $return = $this->getStatusMessage('OK');
            $return['data'] = $data;
            return $this->sendResponse($return);

        }
        catch(\Exception $e) {

        }
    }

    /**
     * @return array
     */
    private function deleteById()
    {
        $affectedRows = $this->store
            ->filter('id', '=', $this->id)
            ->delete();

        return $affectedRows ? ['deleted' => $affectedRows] : ['deleted' => true];
    }
}