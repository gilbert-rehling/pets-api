<?php

/**
 * Namespace
 */
namespace Api\User\Controllers;

/**
 * @uses
 */
use Application\Controllers\AbstractRestController;
use Application\Models\User;
use Exception;

/**
 * Class UserApiController
 * @package Api\User\Controllers
 */
class UserApiController extends AbstractRestController
{
    /**
     * @var $config array (database configuration)
     */
    private $config;

    /**
     * @var User
     */
    private $user;

    /**
     * @var string|null
     */
    private $username = null;

    /**
     * @var string|null
     */
    private $password = null;

    /**
     * @var array|null
     */
    private $params = null;

    /**
     * @param $username
     */
    public function setUsername( $username )
    {
        $this->username = $username;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param $password
     */
    public function setPassword( $password )
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param $params
     */
    public function setParams( $params )
    {
        $this->params = $params;
    }

    /**
     * @return array|null
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * UserApiController constructor.
     *
     * @param array|null    $config
     * @param array|null    $pdo
     * @param array|null    $vars
     * @param array|null    $params
     *
     * @throws Exception
     */
    public function __construct( $config = null, $pdo = null, $vars = null, $params = null )
    {
        if ($config == null) {
            throw new \Exception('No configuration was passed to the constructor');
        }
        $this->config = $config;

        if ($pdo == null) {
            throw new \Exception('Database ORM not available or not configured');
        }
        $this->user = $pdo['user'];

        if ($vars) {
            // !! only accept string as value !!
            $this->setUsername((string) $vars['username']);
        }

        if ($params) {
            $this->setParams( $params );
        }
    }

    /**
     * Index action runs method selection
     */
    public function indexAction()
    {
        try {
            // call the correct method
            if ($this->username) {
                $data = $this->findByUsername();

            } else {
                $data = $this->logInOut();
            }
            // prepare and send the response
            $return = $this->getStatusMessage('OK');
            $return['data'] = $data;
            return $this->sendResponse($return);

        }
        catch(Exception $e) {

        }
    }

    /**
     * @return array
     */
    private function findByUsername()
    {
        $data = $this->user
            ->filter('username', '=', $this->username)
            ->fetch();

        return $data ? $data[0] : [];
    }

    /**
     * @return array
     */
    private function logInOut()
    {
        return [];
    }

}
