<?php

/**
 * Namespace
 */
namespace Api\Pet\Controllers;

/**
 * @uses
 */
use Application\Controllers\AbstractRestController;
use Application\Models\Pet;
use Exception;

/**
 * Class PetApiPutController
 * @package Api\Pet\Controllers
 */
class PetApiPutController extends AbstractRestController
{
    /**
     * @var $config array (database configuration)
     */
    private $config;

    /**
     * @var Pet
     */
    private $pet;

    /**
     * @var integer|null
     */
    private $id = null;

    /**
     * @var array|null
     */
    private $params = null;

    /**
     * @param $id
     */
    public function setId( $id )
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $params
     */
    public function setParams( $params )
    {
        $this->params = $params;
    }

    /**
     * @return array|null
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * PetApiPutController constructor.
     *
     * @param array|null    $config
     * @param array|null    $pdo
     * @param array|null    $vars
     * @param array|null    $params
     *
     * @throws Exception
     */
    public function __construct( $config = null, $pdo = null, $vars = null, $params = null )
    {
        if ($config == null) {
            throw new \Exception('No configuration was passed to the constructor');
        }
        $this->config = $config;

        if ($pdo == null) {
            throw new \Exception('Database ORM not available or not configured');
        }
        $this->pet = $pdo['pet'];

        if ($vars) {
            // !! only accept int as value !!
            $this->setId((int) $vars['id']);
        }

        if ($params) {
            $this->setParams($params);
        }
    }

    /**
     * Index action loads directory data
     */
    public function indexAction()
    {
        // prepare and send the response
        $return = $this->getStatusMessage('OK');
        $return['data'] =  array();
        return $this->sendResponse($return);
    }
}