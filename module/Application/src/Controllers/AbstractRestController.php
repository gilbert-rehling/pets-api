<?php

/**
 * Namespace
 */
namespace Application\Controllers;

/**
 * Class AbstractRestController
 * @package Api\Controllers
 */
abstract class AbstractRestController
{
    /**
     * @var array
     */
    private  $statusMessages = [
        "OK" => [
            "message" => "Operation executed successfully.",
            "code" => 200,
            "type" => "Success"
        ],
        "ConditionNotMet" => [
            "message" => "The condition specified in the conditional header(s) was not met for a read operation.",
            "code" => 304,
            "type" => "Not Modified"
        ],
        "MissingRequiredHeader" => [
            "message" => "A required HTTP header was not specified.",
            "code" => 400,
            "type" => "Bad Request"
        ],
        "UnsupportedHeader" => [
            "message" => "One of the HTTP headers specified in the request is not supported.",
            "code" => 400,
            "type" => "Bad Request"
        ],
        "InvalidHeaderValue" => [
            "message" => "The value provided for one of the HTTP headers was not in the correct format.",
            "code" => 400,
            "type" => "Bad Request"
        ],
        "MissingRequiredQueryParameter" => [
            "message" => "A required query parameter was not specified for this request.",
            "code" => 400,
            "type" => "Bad Request"
        ],
        "UnsupportedQueryParameter" => [
            "message" => "One of the query parameters specified in the request URI is not supported.",
            "code" => 400,
            "type" => "Bad Request"
        ],
        "InvalidQueryParameterValue" => [
            "message" => "An invalid value was specified for one of the query parameters in the request URI.",
            "code" => 400,
            "type" => "Bad Request"
        ],
        "OutOfRangeQueryParameterValue" => [
            "message" => "A query parameter specified in the request URI is outside the permissible range.",
            "code" => 400,
            "type" => "Bad Request"
        ],
        "RequestUrlFailedToParse" => [
            "message" => "The url in the request could not be parsed.",
            "code" => 400,
            "type" => "Bad Request"
        ],
        "InvalidUri" => [
            "message" => "The requested URI does not represent any resource on the server.",
            "code" => 400,
            "type" => "Bad Request"
        ],
        "InvalidHttpVerb" => [
            "message" => "The HTTP verb specified was not recognized by the server.",
            "code" => 400,
            "type" => "Bad Request"
        ],
        "Md5Mismatch" => [
            "message" => "The MD5 value specified in the request did not match the MD5 value calculated by the server.",
            "code" => 400,
            "type" => "Bad Request"
        ],
        "OutOfRangeInput" => [
            "message" => "One of the request inputs is out of range.",
            "code" => 400,
            "type" => "Bad Request"
        ],
        "InvalidAuthenticationInfo" => [
            "message" => "The authentication information was not provided in the correct format. Verify the value of Authorization header.",
            "code" => 400,
            "type" => "Bad Request"
        ],
        "AuthenticationFailed" => [
            "message" => "Server failed to authenticate the request. Make sure the value of the Authorization header is formed correctly including the signature.",
            "code" => 403,
            "type" => "Forbidden"
        ],
        "InsufficientAccountPermissionsRead" => [
            "message" => "Read operations are currently disabled.",
            "code" => 403,
            "type" => "Forbidden"
        ],
        "InsufficientAccountPermissionsWrite" => [
            "message" => "Write operations are currently disabled.",
            "code" => 403,
            "type" => "Forbidden"
        ],
        "ResourceNotFound" => [
            "message" => "The specified resource does not exist.",
            "code" => 404,
            "type" => "Not Found"
        ],
        "AccountIsDisabled" => [
            "message" => "The specified account is disabled.",
            "code" => 403,
            "type" => "Forbidden"
        ],
        "InsufficientAccountPermissions" => [
            "message" => "The account being accessed does not have sufficient permissions to execute this operation.",
            "code" => 403,
            "type" => "Forbidden"
        ],
        "AccountAlreadyExists" => [
            "message" => "The specified account already exists.",
            "code" => 409,
            "type" => "Conflict"
        ],
        "AccountBeingCreated" => [
            "message" => "The specified account is in the process of being created.",
            "code" => 409,
            "type" => "Conflict"
        ],
        "PreConditionNotMet" => [
            "message" => "The condition specified in the conditional header(s) was not met for a write operation.",
            "code" => 412,
            "type" => "Precondition Failed"
        ],
        "InvalidRange" => [
            "message" => "The range specified is invalid for the current size of the resource.",
            "code" => 416,
            "type" => "Requested Range Not Satisfiable"
        ],
        "InternalError" => [
            "message" => "The server encountered an internal error. Please retry the request.",
            "code" => 500,
            "type" => "Internal Server Error"
        ],
        "OperationTimedOut" => [
            "message" => "The operation could not be completed within the permitted time.",
            "code" => 500,
            "type" => "Internal Server Error"
        ],
        "ServerBusy" => [
            "message" => "The server is currently unable to receive requests. Please retry your request.",
            "code" => 503,
            "type" => "Service Unavailable"
        ]
    ];

    /**
     * Method returns StatusMessage valu
     *
     * @param $key
     *
     * @return array|mixed
     */
    public function getStatusMessage ($key)
    {
        if($key && isset($this->statusMessages[$key])){
            return $this->statusMessages[$key];
        }

        return [
            "message" => "The operation could not be completed within the permitted time.",
            "code" => 500,
            "type" => "Service Unavailable"
        ];
    }

    /**
     * Sends or returns the response
     *
     * @param $return
     *
     * @return false|string
     */
    public function sendResponse($return) {
        // for unit testing we don't need the header
        if (php_sapi_name() === 'cli-server' || php_sapi_name() === 'cli') {
            // return the JSON
            return json_encode($return);

        } else {
            header('Content-Type: application/json');
            echo json_encode($return);
        }
    }

    /**
     * Method can be used to sanitise a value or an array
     *
     * @param $value
     *
     * @return array|mixed
     */
    public function sanitizeData( $value )
    {
        if (is_array($value)) {
            $result = array();
            foreach ($value as $key => $val) {
                $result[$key] = (is_array($val) ? self::sanatizeValue($val) : filter_var(trim($val), FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES));
            }
            return $result;

        } else {
            return filter_var(trim($value), FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);
        }
    }

    /**
     * Method can be used to sanitise a request POST array
     *
     * @param array $post
     *
     * @return array
     */
    public function sanitizePost( $post )
    {
        if ($post) {
            $arr = [];
            foreach ($post as $key => $value) {
                $arr[ $key ] = $this->sanitizeData( $value );
            }
            return $arr;
        }
    }
}