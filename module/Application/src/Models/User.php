<?php

/**
 * Namespace
 */
namespace Application\Models;

/**
 * @uses
 */
use Phormium;


/**
 * Class User
 * @package Application\Models
 */
class User extends Phormium\Model
{
    // Mapping meta-data
    protected static $_meta = array(
        'database' => 'petapi',
        'table' => 'user',
        'pk' => 'id'
    );

    // Table columns
    public $id;

    public $username;

    public $first_name;

    public $last_name;

    public $email;

    public $password;

    public $phone;

    public $user_status;

    public $updated_at;

    public $created_at;

    /**
     * Required for unit testing
     *
     * @param $name
     */
    public function overrideDbName( $name )
    {
        if ($name) {
            self::$_meta['database'] = $name;
        }
    }
}