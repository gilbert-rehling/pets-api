<?php

/**
 * Namespace
 */
namespace Application\Models;

/**
 * @uses
 */
use Phormium;


/**
 * Class Pet
 * @package Application\Models
 */
class Pet extends Phormium\Model
{
    // Mapping meta-data
    protected static $_meta = array(
        'database' => 'petapi',
        'table' => 'pet',
        'pk' => 'id'
    );

    // Table columns
    public $id;

    public $category_id;

    public $name;

    public $photo_urls;

    public $tags;

    public $status;

    public $updated_at;

    public $created_at;

    /**
     * Required for unit testing
     *
     * @param $name
     */
    public function overrideDbName( $name )
    {
        if ($name) {
            self::$_meta['database'] = $name;
        }
    }
}