<?php

/**
 * Namespace
 */
namespace Application\Models;

/**
 * @uses
 */
use Phormium;


/**
 * Class Store
 * @package Application\Models
 */
class Store extends Phormium\Model
{
    // Mapping meta-data
    protected static $_meta = array(
        'database' => 'petapi',
        'table' => 'orders',
        'pk' => 'id'
    );

    // Table columns
    public $id;

    public $pet_id;

    public $quantity;

    public $ship_date;

    public $status;

    public $completed;

    public $updated_at;

    public $created_at;

    /**
     * Required for unit testing
     *
     * @param $name
     */
    public function overrideDbName( $name )
    {
        if ($name) {
            self::$_meta['database'] = $name;
        }
    }
}