# Pet Shop Microservice Project

Another RESTful web service project by Gilbert Rehling.  
I'm back to work on my other projects now, and this one has consumed enough of my time.  
I went to the trouble of getting Docker setup on my Linux DEV box, so now I can toy around with distributing some builds
around on my various desktops.

**!! A golang version is taking shape here: (https://github.com/gilbert-rehling/go-api)**

**Notes:**  

* The application is based on: (https://petstore.swagger.io/#/)  
* The Docker configurations are placeholders and have not been tested.  
* This API was developed using PHP directly on a Linux DEV box using php 7.2.* (LAMP)
* All output from the Controllers is handled by the AbstractController, as such there is no 'v' in our MVC
* At first I used FaaZp/Pdo then changed to Phormium/Phormium which has a nice Model abstraction.  
* The .env file is there for the Docker build - not for the application


**Completion:**  
The Store controllers all have basic functionality completed.
All the routes should resolve to one of the controllers.
The indexAction() on all User controllers will call an appropriate method, but are as yet not all completed.  

Only the primary GET requests tested using Firefox.
Full public access to all functions. (no authentication implemented)  


**Testing:**  
Unit tests have so far only been implemented on the /module/Api/Store/Controllers/StoreApiController.php class
Current tests use a dummy (testing) database - which uses the db_test.json configuration.
There is also a DB_TEST constant in the bootstrap file that can be reused on subsequent tests.
There is also an override method for the model that replaces the dbname in each model metadata  
 -- this needs to be called for each test
 
Test can be triggered from the app root or inside the /test/unit dir.
The shell script does not load due to environmental issues.  

Time restraints prevented be from correctly implementing PHP Phake, which I like to use for mocking.  
There seemed to be some bug affecting its behavior.  


**Hindsights and Todos:**  

* The check for the config's existence could have been handled within the index.php before loading the controllers, instead of inside each controllers constructor - esp. considering its not doing much as yet.
* Adding a global helper class which contains the functions from the index.php script.
* Add a view handler for the JSON responses
* I'm sure other obvious improvements will also come to light.
* More time would be needed to finish the whole thing off, especially the auth side of things.


Copyright (c) 2020 Gilbert Rehling (www.gilbert-rehling.com)  
No rights reserved :)

